var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var throttle = 1;
var track;
var lastAngle = 0;
var tick;
var lastPosition, lastPiecePosition = 0;
var speed;

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {

    // TODO: Calculate per piece only
    position = data.data[0].piecePosition.pieceIndex * 100 + data.data[0].piecePosition.inPieceDistance;

    if(lastPiecePosition<data.data[0].piecePosition.inPieceDistance) speed = position-lastPosition / (data.gameTick-tick);
    console.log('Speed  ', speed);

    if(speed>7 && Math.round(data.data[0].angle*10)!=0) throttle=0.0;

    if((data.data[0].angle>0 && data.data[0].angle<lastAngle) || (data.data[0].angle<0 && data.data[0].angle>lastAngle)) {
      if(data.data[0].angle>0 && data.data[0].angle<lastAngle) throttle+=0.15;
      if(data.data[0].angle<0 && data.data[0].angle>lastAngle) throttle+=0.15;
    } else {
      if(data.data[0].angle>5 || data.data[0].angle<-5) throttle=0.15;
      else if(data.data[0].angle>25 || data.data[0].angle<-25) throttle=0.05;
      else if(throttle<1) throttle+=0.1;
    }
    
    if(speed<4 && (data.data[0].angle<30 && data.data[0].angle>-30)) throttle+=0.3;

    if(speed>8.5) throttle = 0;

    if(throttle>1) throttle=1;


    console.log('Throttle  ', throttle);
    //console.log(throttle);
    send({
      msgType: "throttle",
      data: throttle
    });
    //console.log('Throtling');
    tick = data.gameTick;
    lastPosition = position;
    lastPiecePosition=data.data[0].piecePosition.inPieceDistance;
    lastAngle = data.data[0].angle;
    console.log(data.data);
    return;
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
      console.log(data);
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if(data.msgType === 'gameInit') {
      console.log(data.data.race.track)
      track = data.data.race.track.pieces;
    } else if(data.msgType === 'crash') {
      console.log('CRASH!');
    } else if(data.msgType === 'spawn') {
      console.log('SPAWN');
      send({
        msgType: "throttle",
        data: throttle
      });
      return;
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
